<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $object = new Animal("shaun");

    echo "Name : ".$object->name."<br>";
    echo "Legs : ".$object->legs."<br>";
    echo "cold blooded : ".$object->cold_blooded."<br>";
    echo "<br><br>";

    $object2 = new Frog("buduk");

    echo "Name : ".$object2->name."<br>";
    echo "Legs : ".$object2->legs."<br>";
    echo "cold blooded : ".$object2->cold_blooded."<br>";
    echo $object2->jump();
    echo "<br><br>";

    $object3 = new Ape("Kera Sakti");

    echo "Name : ".$object3->name."<br>";
    echo "Legs : ".$object3->legs."<br>";
    echo "cold blooded : ".$object3->cold_blooded."<br>";
    echo $object3->yell();

?>
