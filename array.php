<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* SOAL NO 1                   */
    
    $kids=["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"]; // Lengkapi di sini
    echo("data kids : ");    
    print_r($kids);
    $adults=["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    echo("<br>");
    echo("data addults : ");
    print_r($adults);
    
    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: ". count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    

    echo "</ol>";

    echo "Total Adults: ". count($adults); // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    


    echo "</ol>";

    echo "<h3> Soal 3</h3>";
    $bio =[
        ["name"=>"will byers","age"=>"12","aliases"=>"will the wise","status"=>"Alive"],
        ["name"=>"Mike wheeler","age"=>"12","aliases"=>"Dungeon Master","status"=>"Alive"],
        ["name"=>"jim Hooper","age"=>"43","aliases"=>"Chief Hopper","status"=>"Deceased"],
        ["name"=>"Eleven","age"=>"12","aliases"=>"El","status"=>"Alive"]
    ];
    
        echo"<pre>";
        print_r($bio);
        echo"</pre>";
    ?>
</body>

</html>